# RF DSP Introduction and Practical Examples in Python

This project uses Python 3 and a list of additional packages that are provided in an Anaconda environment configuration file.  

Disk space requirement for this project is 1.2 GB (50 MB Miniconda + 1 GB conda environment + 50 MB data files)

The project has been tested on Ubuntu Linux 18.04 (Bionic), Debian 9.8 Linux (Stretch), Mac OSX 10.14 (Mojave), and Windows 10 Home (using Powershell) 

These instructions require the use of Terminal in Linux / Mac or Powershell/CMD in Windows.  Commands to type in the terminal are shown as:

` $ command <parameter> <parameter>`

Example outputs to terminal screen are shown as:

```
Example output
on Terminal screen
```

# Setup Instructions

## Install Miniconda Environment Manager

1. If you already have Anaconda or Miniconda environment manager installed, continue to next section **Download the source code for this project** 

1. If you do not yet have Anaconda / Miniconda, download and install Miniconda with Python 3.7.  File size is 50 MB. https://docs.conda.io/en/latest/miniconda.html

## Configure Conda to not activate on startup

`$ conda config --set auto_activate_base false`

## Download the source code for this project

1. If you already use git,`$ git clone https://gitlab.com/narit-raoc/course.git`
    
1. If you don't use git, use link to download ZIP from Gitlab website 

https://gitlab.com/narit-raoc/course/-/archive/master/course-master.zip 

Extract to the directory of your choice.

#   Check files
Confirm that you can see the files in this list

` $ cd course/rfdsp`

` $ ls -l`

```
--rw-r--r-- 1 ssarris ssarris  183 Jun 10 20:27 env_rfdsp.yml
-rw-r--r-- 1 ssarris ssarris 1.1K Jun 10 20:27 generate_all.py
-rw-rw-r-- 1 ssarris ssarris  933 Jun 10 20:30 generate_ex1.py
-rw-rw-r-- 1 ssarris ssarris  937 Jun 10 20:33 generate_ex2.py
-rw-rw-r-- 1 ssarris ssarris 1.3K Jun 10 20:33 generate_ex3.py
-rw-rw-r-- 1 ssarris ssarris 1.7K Jun 10 20:33 generate_ex4.py
-rw-r--r-- 1 ssarris ssarris 1.4K Jun 12 10:39 generate_ex5.py
-rw-r--r-- 1 ssarris ssarris 1.1K Jun 11 22:25 process_ex1.py
-rw-r--r-- 1 ssarris ssarris 1.2K Jun 11 22:29 process_ex2.py
-rw-r--r-- 1 ssarris ssarris  980 Jun 11 22:28 process_ex3.py
-rw-r--r-- 1 ssarris ssarris 1.1K Jun 12 11:32 process_ex4.py
-rw-r--r-- 1 ssarris ssarris 1.2K Jun 10 20:56 process_ex5.py
-rw-r--r-- 1 ssarris ssarris 1.2K Jun 12 10:42 README.md
-rw-r--r-- 1 ssarris ssarris 1.7M Jun 12 10:43 rfdsp.pdf
-rw-r--r-- 1 ssarris ssarris  15K Jun 10 21:33 rfutil.py

```

# Create the development environment from the configuration file

`$ conda env create -f env_rfdsp.yml`

This command `create` will download all of the Python packages from the list in env_rfdsp.yml, create and environment with name `env_rfdsp` to access the required Python packages.

Wait 5-10 minutes until finished.

# Activate the environment

`$ conda activate env_rfdsp`

After active,  you should see the name of the environment on the left side shown as `(env_rfdsp)`

```
(env_rfdsp) ssarris@ssarris-MacBookPro:~/src/course/rfdsp$ 
```
# Generate the data files to use for this project

`$ python generate_all.py`

After the Python script to generate files is complete, check list of files again and check to see 6 example data files.

`ls -l`
```
-rw-r--r-- 1 ssarris ssarris      240 Jun 16 12:50 ex1.iq
-rw-r--r-- 1 ssarris ssarris      240 Jun 16 12:50 ex2.iq
-rw-r--r-- 1 ssarris ssarris      240 Jun 16 12:50 ex3.iq
-rw-r--r-- 1 ssarris ssarris   160000 Jun 16 12:50 ex4.iq
-rw-r--r-- 1 ssarris ssarris 16000000 Jun 16 12:50 ex5.iq
-rw-r--r-- 1 ssarris ssarris  4800000 Jun 16 12:50 ex6.iq
```
# Start the activity
Open the file rfdsp.pdf to start the course and examples

# Record a real signal with Ettus SDR
After you finish the examples and understand the graphs, we will use the Ettus E312 SDR with a basic narrowband receiver software to record some real signals from outside.  You can keep the data files and analyze later using the techniques we learned in example 1-5.