import numpy as np
import matplotlib
matplotlib.use("QT5Agg")
import matplotlib.pyplot as plt
plt.style.use('dark_background')
import scipy.signal
import commpy.filters

speed_of_light = 299792458

def siggen_noise(fs, length_s):
	n_samples = int(length_s*fs)
	tt = np.arange(0,n_samples)/fs
	noise = np.random.normal(size=n_samples) + 1j*np.random.normal(size=n_samples)
	return(noise)

def siggen_cosine(fs, length_pulse_s, freq, phi0=0, window=None):
	''' Generate a cosine pulse of duration length_pulse_s (in seconds)
	Apply tukey window to the pulse to soft-start and end the pulse
	This window in the time domain results in cleaner spectrum'''

	# Calculate number of samples required to generate this pulse
	n_samples = int(length_pulse_s*fs)
	# Calculate values of time samples
	tt = np.arange(0,n_samples)/fs
	# Generate a CW tone for the time samples in tt
	# Note: exp(1j*phi) = cos(phi)+1j*sin(phi).  
	# Convenient complex-value notation in Python
	sig_c = np.exp(1j*(2*np.pi*freq*tt+phi0))

	if window is not None:
		w = scipy.signal.get_window(window, n_samples)
	else:
		w = np.ones(n_samples)

	return w*sig_c+np.spacing(1)
	# Add smallest floating point number np.spacing(1) to avoid warnings
	# about divide-by-zero in function np.log10 (logarithmic scale)
	
def siggen_bpsk(fs, length_pulse_s, freq, symbol_rate, debug=False):
	# Calculate number of samples required to generate this pulse
	n_samples = int(length_pulse_s*fs)
	n_symbols = int(length_pulse_s*symbol_rate)
	sps = int(fs/symbol_rate)
	print('n_samples in %d' % n_samples)
	print('length_pulse_s: %f' % length_pulse_s)
	print('symbol_rate: %f' % symbol_rate)
	print('samples per symbol (sps): %d' % sps)
	print('n_symbols: %d' % n_symbols)
	
	# Create sequence of random bits (data to transmit)
	bitstream = np.random.randint(low=0, high=2, size=n_symbols)	
	# Upsample to actual sample rate.  In this example, sps is an integer.
	# For sps values that are not integer, use scipy.signal.resample or 
	# scipy.signal.resample_poly for rational resampler.
	bitstream_fs = np.repeat(bitstream, sps)
	print('length of bitstream_fs %d: ' % bitstream_fs.shape[0])
	
	# Use this bitstream as phase modulation on baseband (freq=0) signal
	waveform_bb = np.exp(1j*np.pi*bitstream_fs)
	print('length of waveform_bb %d: ' % waveform_bb.shape[0])
	
	# Create a raised-cosine / Hann window to clean up the spectrum
	n_taps = sps*10
	alpha = 0.4
	Ts = 1/float(symbol_rate)
	h_rc = commpy.filters.rcosfilter(n_taps, alpha, Ts, fs)[1]
	
	# Apply the FIR filter to the baseband signal before frequency shifting to RF
	waveform_bb_filtered = np.convolve(waveform_bb, h_rc)/n_taps
	print('length of waveform_bb_filtered %d: ' % waveform_bb_filtered.shape[0])
	
	# Calculate values of time samples
	tt = np.arange(0, waveform_bb_filtered.shape[0])/fs
	waveform_rf = waveform_bb_filtered*np.exp(1j*(2*np.pi*freq*tt))
	#waveform_rf = waveform_bb_filtered[int(n_taps/2):int(-1*(n_taps/2)+1)]*np.exp(1j*(2*np.pi*freq*tt))
	
	if debug is True:
		#fft_size = 1024
		spec_h_rc = np.fft.fft(h_rc)
		spec_waveform_bb_filt = np.fft.fft(waveform_bb_filtered)
		spec_waveform_rf = np.fft.fft(waveform_rf)

		# Plot graphs
		figure_size = (8, 4)
		fig1 = plt.figure(1, figsize=figure_size)
		ax1a = plt.subplot(3,1,1)
		ax1a.set_title('bitstream')
		ax1a.plot(bitstream,'yo')
		ax1a.grid(True, which='both')

		ax1b = plt.subplot(3,1,2)
		ax1b.set_title('bitstream_fs')
		ax1b.plot(h_rc,'y-')
		
		ax1c = plt.subplot(3,1,3)
		ax1c.set_title('waveform_bb')
		ax1c.plot(np.real(waveform_bb),'c-')
		ax1c.plot(np.imag(waveform_bb),'m-')
		ax1c.grid(True, which='both')

		fig2 = plt.figure(2, figsize=figure_size)
		ax2a = plt.subplot(2,1,1)
		ax2a.set_title('magnitude of baseband')
		ax2a.plot(np.abs(waveform_bb),'y-')
		ax2a.grid(True, which='both')

		ax2b = plt.subplot(2,1,2)
		ax2b.set_title('phase of baseband')
		ax2b.plot(np.angle(waveform_bb),'y-')
		ax2b.grid(True, which='both')

		fig3 = plt.figure(3, figsize=figure_size)
		ax3a = plt.subplot(2,1,1)
		ax3a.set_title('real/imag of rf')
		ax3a.plot(np.real(waveform_rf),'c-')
		ax3a.plot(np.imag(waveform_rf),'m-')

		ax3a.grid(True, which='both')
		ax3b = plt.subplot(2,1,2)
		ax3b.set_title('window function')
		ax3b.plot((h_rc),'y-')
		ax3b.grid(True, which='both')

		fig4 = plt.figure(4, figsize=figure_size)
		ax4a = plt.subplot(1,1,1)
		ax4a.set_title('FFT spectrum of RF signal')
		ax4a.plot(20*np.log10(np.abs(spec_waveform_bb_filt)),'c-')
		ax4a.plot(20*np.log10(np.abs(spec_waveform_rf)),'m-')
		#ax4a.set_ylim((-100, 100))
		ax4a.grid(True, which='both')
		plt.show()

	return waveform_rf[0:n_samples]

def siggen_chirp(fs, length_pulse_s, freq, bw):
		sample_period = 1 / fs
		length_samples = int(length_pulse_s * fs)
		
		tt_sec = np.linspace(0, length_pulse_s, length_samples, endpoint=False)
		freq_slope = bw / length_pulse_s

		# Phase is the integral of frequency.  We want a linear FM chirp
		# Therefore, the phase trajectory is ^2 exponential
		phase_trajectory = 2 * np.pi * ((freq-bw/2) * tt_sec + 0.5 * freq_slope * (tt_sec**2))

		# Generate a complex sinusoid with calculated phase
		chirp_tt = np.exp(1j*phase_trajectory)

		# Make a window function to soft-start the waveform.
		# This window is necessary to soft-start the signal to pass through
		# analog elctronics that have finite bandwidth.
		# This window also reduces sidelobes in cross-correlation
		# to calculate range-compressed pulse
		w = scipy.signal.tukey(length_samples)
		chirp_tt_win = chirp_tt*w
		return chirp_tt_win

def siggen_doppler_flyby(fs, length_pulse_s, freq, velocity, debug=False):
		
		sample_period = 1 / fs
		length_samples = int(length_pulse_s * fs)
		
		# array of time samples. linear spacing
		tt_sec = np.arange(length_samples)*sample_period-0.5*length_pulse_s
		# tt_sec = np.linspace(-length_pulse_s/2, length_pulse_s/2, length_samples, endpoint=True)+0.5
		# Create an array of distances to calculate angle of arrival.  add small value np.spacing(1)
		# to avoid error of divide-by-zero
		max_distance = 4
		position = np.linspace(-1*max_distance, max_distance, length_samples)
		height = 1
		theta_EL_tt = np.arctan2(height,position)
				
		# Calculate frequency as a function time with doppler shift.
		f_doppler_tt =  -1*freq*(velocity/speed_of_light)*np.cos(theta_EL_tt)
		freq_tt = freq+f_doppler_tt
		phase_trajectory = (2 * np.pi) * (freq_tt * tt_sec)

		# Generate a complex sinusoid with calculated phase
		signal_tt = np.exp(1j*phase_trajectory)
		if(debug):
			f = plt.figure(1)
			ax1 = plt.subplot(3,1,1)
			ax1.plot(theta_EL_tt)
			ax2 = plt.subplot(3,1,2)
			ax2.plot(phase_trajectory)
			ax3 = plt.subplot(3,1,3)
			ax3.plot(np.diff(phase_trajectory))
			plt.show()
			print('tt[0]: %f tt[-1]: %f' % (tt_sec[0], tt_sec[-1]))
		return signal_tt

def accumulate(fs, delay_s, sig_accumulator, sig_new):
	# Calculate number of samples delay
	n_start = int(fs*delay_s)
	# Calculate end of sig_add with delay and see if it can fit in sig_base
	n_stop = n_start + sig_new.shape[0]
	print('sig_accumulate size %d' % sig_accumulator.shape[0])
	print('sig_new size %d' % sig_new.shape[0])
	print('delay_s %f' % delay_s)
	print('n_start %d' % n_start)
	print('n_stop %d' % n_stop)
	if(n_stop > sig_accumulator.shape[0]):
		print('sig_new goes past end of sig_accumulator.  Cannot accumulate')
		return
	else:
		sig_accumulator[n_start:n_stop] += sig_new
		return sig_accumulator

def channelize_fft(signal_t_1D, fft_size):
	# Caluclate number of columns to split the long 1-D array.
	n_timebins = int(np.floor(float(signal_t_1D.size) / fft_size))
	print('n segments (time bins) of FFT size in signal_t_1D: %d' % n_timebins)
	
	# Truncate the signal so that the total length can divide by FFT size exactly
	signal_t_1D_truncated = signal_t_1D[0:int(n_timebins*fft_size)]
	print('length of signal_t_1D_truncated: %s' % signal_t_1D_truncated.shape[0])

	# Reshape the 1-D array into 2-D array with each column the 
	# same length as FFT sizes
	signal_t_2D = np.reshape(signal_t_1D_truncated, (n_timebins, fft_size))
	print('length of signal_t_2D dimension 0: %s' % signal_t_2D.shape[0])
	print('length of signal_t_2D dimension 1: %s' % signal_t_2D.shape[1])

	# Generate a window function for apodization
	#w = scipy.signal.windows.get_window('boxcar',fft_size)
	w = scipy.signal.windows.get_window('hann',fft_size)	

	# Tile the window across all rows and apply window
	w_2D = np.tile(w,(n_timebins,1))
	signal_t_2D_w = w_2D*signal_t_2D
	
	# Calculate FFT of each column
	data_cube = np.fft.fft(signal_t_2D_w, fft_size, axis=1) / fft_size
	print('length of channelized data cube dimension 1 (time): %s' % data_cube.shape[0])
	print('length of channelized data cube dimension 2 (freq): %s' % data_cube.shape[1])	
	
	return data_cube

def analyze_iq(Ts, Ycal, yy):
	# Scale x (time) axis and y (voltage) to translate to physical units
	xx = np.arange(0,yy.shape[0])
	xcal = Ts*xx
	xcal_ms = xcal*1e3
	ycal = Ycal*yy

	# Make plots
	figure_size = (11,7)
	fig1 = plt.figure('analyze_iq', figsize=figure_size)

	ax1 = plt.subplot2grid((2,2), (0,0), rowspan=1, colspan=1)
	ax1.plot(xx, np.real(yy),'b--')
	ax1.plot(xx, np.real(yy),'bo', label='real')
	ax1.plot(xx, np.imag(yy),'r--')
	ax1.plot(xx, np.imag(yy),'ro', label='imag')
	ax1.set_title('Complex-Valued Signal. Ts=%5.2f [msec], Ycal=%5.2f[Volts/1.0]' % (Ts*1e3, Ycal))
	ax1.set_xlabel('Sample Number [integer]')
	ax1.set_ylabel('Amplitude [no unit]')
	ax1.grid(True, which='both')
	ax1.legend(loc=3)

	ax2 = plt.subplot2grid((2,2), (1,0), rowspan=1, colspan=1)
	ax2.plot(xcal_ms, np.real(ycal),'b--')
	ax2.plot(xcal_ms, np.real(ycal),'bo', label='real')
	ax2.plot(xcal_ms, np.imag(ycal),'r--')
	ax2.plot(xcal_ms, np.imag(ycal),'ro', label='imag')
	#ax2.set_title('Complex-Valued Signal')
	ax2.set_xlabel('Time [msec]')
	ax2.set_ylabel('Amplitude [volts]')
	ax2.grid(True, which='both')
	ax2.legend(loc=3)

	ax3 = plt.subplot2grid((2,2), (0,1), rowspan=2, colspan=1)
	ax3.plot(np.real(yy), np.imag(yy),'x')
	ax3.set_aspect('equal')
	ax3.set_title('Complex Phasor Constellation')
	ax3.set_xlabel('I (real)')
	ax3.set_ylabel('Q (imag)')
	ax3.grid(True, which='both')
	plt.show()

def analyze_mag_phase(Ts, Ycal, yy, xlabel='', ylabel='', blocking=True):
	# Scale x (time) axis and y (voltage) to translate to physical units
	xx = Ts*np.arange(0,yy.shape[0])
	yy = Ycal*yy

	yy_mag = np.abs(yy)
	yy_phs = np.angle(yy)

	# Make plots
	figure_size = (8,10)
	fig1 = plt.figure('analyze_mag_phase', figsize=figure_size)
	
	ax1 = plt.subplot(3,1,1)
	ax1.plot(xx, np.real(yy),'b--')
	ax1.plot(xx, np.real(yy),'bo', label='real')
	ax1.plot(xx, np.imag(yy),'r--')
	ax1.plot(xx, np.imag(yy),'ro', label='imag')
	ax1.set_title('Complex-Valued Signal')
	ax1.set_xlabel(xlabel)
	ax1.set_ylabel(ylabel)
	ax1.grid(True, which='both')
	ax1.legend(loc=3)

	ax2 = plt.subplot(3,1,2)
	ax2.plot(xx, yy_mag,'b--')
	ax2.plot(xx, yy_mag,'bo')
	ax2.set_xlabel(xlabel)
	ax2.set_ylabel('Magnitude (linear scale)')
	ax2.grid(True, which='both')

	ax3 = plt.subplot(3,1,3)
	ax3.plot(xx, yy_phs,'b--')
	ax3.plot(xx, yy_phs,'bo')
	ax3.set_xlabel(xlabel)
	ax3.set_ylabel('Phase (radians)')
	ax3.grid(True, which='both')
	
	plt.draw()
	if(blocking is True):
		plt.show()

def analyze_phase_slope(fs, yy, blocking=True):
	# Use phase slope to calculate frequency.
	yy_phs = np.angle(yy)
	slope_rad_per_sample = np.diff(yy_phs)

	# Adjust for periodic phase wraps for both negative slope and positive slope
	i_wrap_neg = np.where(slope_rad_per_sample < -1*np.pi)
	i_wrap_pos = np.where(slope_rad_per_sample > np.pi)
	print('Adjusting phase wraps +pi to -pi (positive slope). count: %d' % i_wrap_neg[0].shape[0])
	slope_rad_per_sample[i_wrap_neg[0]] += 2*np.pi

	print('Adjusting phase wraps -pi to +pi (negative slope). count: %d' % i_wrap_pos[0].shape[0])
	slope_rad_per_sample[i_wrap_pos[0]] -= 2*np.pi

	i_wrap_nyquist_zone_2 = np.where(slope_rad_per_sample < 0)
	
	print('Wrap (-)frequency to (+)frequency 2nd Nyquist zone. count: %d' % i_wrap_nyquist_zone_2[0].shape[0])
	slope_rad_per_sample[i_wrap_nyquist_zone_2[0]] = slope_rad_per_sample[i_wrap_nyquist_zone_2[0]]+2*np.pi

	print('Median slope radians per sample: %f' % np.median(slope_rad_per_sample))

	# Make plots
	figure_size = (8,6)
	fig1 = plt.figure('analyze_phase_slope', figsize=figure_size)
	
	ax1 = plt.subplot(2,1,1)
	ax1.plot(yy_phs,'b--')
	ax1.plot(yy_phs,'bo')
	ax1.set_title('Sample Rate = %8.3f [kHz]' %(fs/1e3))
	ax1.set_xlabel('Sample Number')
	ax1.set_ylabel('Phase (radians)')
	ax1.grid(True, which='both')

	ax1= plt.subplot(2,1,2)
	ax1.plot(slope_rad_per_sample,'b--')
	ax1.plot(slope_rad_per_sample,'bo')
	ax1.set_ylim((0, 2*np.pi))
	ax1.set_xlabel('Sample Number')
	ax1.set_ylabel('Phase Slope (radian / sample)')
	ax1.grid(True, which='both')

	plt.draw()
	if(blocking is True):
		plt.show()

def analyze_spectrum(fs, Ycal, yy):
	fft_size = yy.shape[0]
	df = fs/fft_size
	ff = np.arange(fft_size)*df
	#w = scipy.signal.windows.get_window('blackmanharris',fft_size)
	w = np.ones(fft_size)
	yy_ff = np.fft.fft(w*yy)/fft_size
	yy_ff_cal = yy_ff*Ycal
	
	yy_ff_mag = 20*np.log10(np.abs(yy_ff))
	yy_ff_mag_cal = 20*np.log10(np.abs(yy_ff_cal))

	ff_kHz = ff/1e3
	ff_shift_kHz = (ff-fs/2)/1e3
	yy_ff_mag_shift = np.fft.fftshift(yy_ff_mag_cal)

	figure_size = (8,10)
	fig1 = plt.figure('analyze_spectrum_nowrap', figsize=figure_size)
	ax1 = plt.subplot(2,1,1)
	ax1.set_title('FFT Magnitude Spectrum. fs = %5.2f kHz, n_fft = %d' % (1e-3*fs, fft_size))
	ax1.plot(yy_ff_mag,'b-')
	ax1.set_xlim((0, fft_size))
	ax1.set_xlabel('FFT Channel Index [int] - direct out of FFT')
	ax1.set_ylabel('Magnitude [dB arbitrary]')
	ax1.grid(True, which='both')

	# ax2 = plt.subplot(1,1,1)		
	# ax2.set_title('FFT Magnitude Spectrum. fs = %5.2f kHz, n_fft = %d' % (1e-3*fs, fft_size))
	# ax2.plot(ff_kHz,yy_ff_mag_cal,'b-')
	# ax2.set_xlim((ff_kHz[0], ff_kHz[-1]))
	# ax2.set_xlabel('Freqeuncy [kHz] 2nd Nyquist Zone not wrapped to (-)')
	# ax2.set_ylabel('Magnitude [dBm]')
	# ax2.grid(True, which='both')
	
	#fig2 = plt.figure('analyze_spectrum_wrap', figsize=figure_size)
	ax3 = plt.subplot(2,1,2)		
	ax3.set_title('FFT Magnitude Spectrum. fs = %5.2f kHz, n_fft = %d' % (1e-3*fs, fft_size))
	ax3.plot(ff_shift_kHz,yy_ff_mag_shift,'b-')
	ax3.set_xlim((ff_shift_kHz[0], ff_shift_kHz[-1]))
	ax3.set_xlabel('Freqeuncy [kHz] 2nd Nyquist Zone Wrapped to (-)')
	ax3.set_ylabel('Magnitude [dBm]')
	ax3.grid(True, which='both')
	
	# # Show all plots
	plt.show()

def analyze_spectrogram(fs, yy, channel_count, dynamic_range=80, nxticks=4, nyticks=11, blocking=True):
	data_cube = channelize_fft(yy, channel_count)
	data_cube_nyquist_wrapped = np.fft.fftshift(data_cube, axes=(1,))

	# Calculate values for time and frequency axes
	dt = channel_count/fs
	tmax = dt*data_cube.shape[0]
	fmax = fs/2
	print('dt=%f, tmax=%f, fmax=%f' % (dt, tmax, fmax))
	
	xticks = np.linspace(0, data_cube.shape[0], nxticks)
	xticklabels = np.linspace(0, tmax, nxticks)
	yticks = np.linspace(0, data_cube.shape[1], nyticks, endpoint=True)
	yticklabels = np.linspace(-fs/2, fs/2, nyticks, endpoint=True)/1e3

	figure_size = (8, 6)
	fig1 = plt.figure('analyze_spectrogram', figsize=figure_size)
	ax1 = plt.subplot(1,1,1)
	# Generate image from intentsity data and choose color map from
	# https://matplotlib.org/tutorials/colors/colormaps.html
	intensity = (20*np.log10(np.abs(data_cube_nyquist_wrapped))).T
	colorhigh = np.max(intensity)
	colorlow = colorhigh-dynamic_range
	extent = (0, 10*tmax, -1*fs/2/1e3, fs/2/1e3)
	im = ax1.imshow(intensity, origin='lower', cmap=matplotlib.cm.get_cmap('inferno'), vmax=colorhigh, vmin=colorlow)

	ax1.set_yticks(yticks)
	ax1.set_yticklabels(yticklabels)
	ax1.set_xticks(xticks)
	ax1.set_xticklabels(xticklabels)
	ax1.set_xlabel('Time [sec]')

	ax1.set_ylabel('Frequency [kHz]')
	ax1.grid(False)
	fig1.colorbar(im, ax=ax1)

	plt.draw()
	if(blocking is True):
		plt.show()

def analyze_wideband(fs, sig_c, channel_count, channel_select, timebin_select, dynamic_range=80):
	
	data_cube = channelize_fft(sig_c, channel_count)
	
	# Select the slice to analyze in each dimension
	print('Select time series data slice from frequency channel: %d ' % channel_select)
	slice_time_series = data_cube[:,channel_select]

	print('Select frequency spectrum data slice from time bin: %d: ' % timebin_select)
	slice_freq_spectrum = data_cube[timebin_select,:]

	# Narrowband sample rate in channel
	fs_channel = fs/channel_count
	print('Sample rate of single channel data fs_channel = %f' % fs_channel)

def write_iq_file(filename_out, sig):
	# Write numpy ndarray data to file.  ndarray.tofile() writes the real and imaginary 
	# components as separate floating point values.  
	
	# Convert to 32-bit floating point array for compatibility with
	# data files written by GNURadio File Sink (default parameters).  Have to separate
	# complex numbers into real/imag components before convert from 64-bit to 32-bit
	rdata = np.real(sig)
	idata = np.imag(sig)
	data64 = np.zeros(2*sig.size)
	data64[0::2] = rdata
	data64[1::2] = idata
	data32 = data64.astype(np.float32)
	file_out=open(filename_out, 'w')
	print("Writing file: name=%s, array length=%d, bytes=%d" % (filename_out, data32.shape[0], data32.shape[0]*4))
	data32.astype(np.float32).tofile(file_out)
	file_out.close()